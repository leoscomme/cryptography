import com.tima.crypto.CryptString;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.util.List;
import java.util.Iterator;


public class DictionaryAttackParallel extends Thread {
    private String encryptedPassword;//passwordCifrata
    private CryptString encoder;
    private String[] commonPasswordsArray;
    private String decryptedPassword;
    private long startTime = 0;
    private long endTime = 0;
    private long startTimeOverhead=0;
    private long endTimeOverhead=0;

    private String salt;
    private List<String> commonPasswordsToConvert;


    //thread-specific attributes
    private int chunkSize;
    private int id;
    private static boolean foundIt = false;


    public DictionaryAttackParallel(String password, String salt, String secretKey, int chunkSize, int id, List<String> commonPasswordsToConvert) throws Exception {
        this.encryptedPassword = password;
        this.chunkSize = chunkSize;
        this.id = id;
        this.salt=salt;
        foundIt = false;
        this.commonPasswordsToConvert = commonPasswordsToConvert;
        DESKeySpec key = new DESKeySpec(secretKey.getBytes());
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        encoder = new CryptString(keyFactory.generateSecret(key));
    }



    public long getElapsedTime() {
        return endTime - startTime;
    }
    public long getOverheadTime(){return  endTimeOverhead- startTimeOverhead;}
    public void run() {
        try {
            startTime=System.currentTimeMillis();
            startTimeOverhead=System.currentTimeMillis();
            commonPasswordsArray = new String[chunkSize];
            Iterator<String> itr = commonPasswordsToConvert.iterator();
            int j = 0;
            while (j < chunkSize && itr.hasNext()) {

                commonPasswordsArray[id*chunkSize+j] = itr.next();
                j++;
            }
            endTimeOverhead= System.currentTimeMillis();
            for (int i = 0; i < commonPasswordsArray.length; i++) {
                if(foundIt){
                    return;
                }

                if (isEqual(commonPasswordsArray[i])) {
                    endTime = System.currentTimeMillis();
                    decryptedPassword = commonPasswordsArray[i];
                    System.out.println("La password è : " + decryptedPassword+" trovata dal thread "+id+" in "+getElapsedTime()+" ms");
                    foundIt = true;

                }
            }
            endTime = System.currentTimeMillis();
            decryptedPassword = null;
            System.out.println("Dictionary attack failed");
        } catch (Exception e) {

        }
    }
    public boolean isEqual(String aString){
        try {
            String toCrypt= aString + salt;
            String criptedString = encoder.encryptBase64(toCrypt);
            if(criptedString.compareTo(encryptedPassword)==0)
                return true;
            else
                return false;

        }catch (Exception e) {
            System.err.println("Error:"+e.toString());
        }
        return false;
    }

    public String getDecryptedPassword() {
        return decryptedPassword;
    }



}
