import com.tima.crypto.CryptString;


import javax.crypto.*;
import javax.crypto.spec.*;
import javax.swing.plaf.synth.SynthTextAreaUI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DBAttackerSequential {
    //Database attacked
    private DataBaseVictim dataBaseVictim;


    //Database  data
    private String encryptedPassword;
    private String secretKey;
    private String salt;

    //Provides methods to encrypt and decrypt strings
    private CryptString encoder;

    //num of char in the password String
    private int passwordSize;

    //Search result
    private String decryptedPassword;

    //Attributes of dictionary attack
    private String [] commonPasswordsArray;
    private long startTimeOverhead;
    private long endTimeOverhead;
    //Number of possible char each element of the string can assume
    private double numOfChars;

    //Starting point for the bruteForceElegant() method, in sequential case is always zero.
    private double initialChunk;

    //First value from which bruteForceTrivial() starts iterating, in sequential case is always zero
    private char[] initValue;

    //Max number of iterations to be performed to find the password
    private double numOfIteration;


    //Perfomance related data
    private long startTime;
    private long endTime;

    //Constructor. Many fields are constant, serves to maintain some symmetry with the Parallel version
    public DBAttackerSequential(DataBaseVictim dataBaseVictim, int passwordSize){
        this.dataBaseVictim=dataBaseVictim;
        this.initialChunk=0;
        this.passwordSize = passwordSize;
        numOfChars=64;
        numOfIteration= Math.pow(numOfChars, passwordSize);
        startTime=0;
        endTime=0;
        numOfChars =64;
        initValue= new char[passwordSize];
        for (int i=0; i< initValue.length; i++){
            initValue[i]= 'a';// usare '' per i char, "" per strighe
        }
    }

    //Simulates the SQLInjection and "steals" data from the DB and initialize the encoder with these information

    public void attackDatabase() throws Exception{

        System.out.println("Attacking the " +dataBaseVictim.getName()+ " database with a SQL Injection...");

        secretKey= dataBaseVictim.getSecretKey();
        encryptedPassword = dataBaseVictim.getHashedPassword();
        salt= dataBaseVictim.getSalt();

        System.out.println("Retrived information: secret key : "+secretKey+",  password :"+ encryptedPassword +",  salt : "+salt);
        DESKeySpec key = new DESKeySpec(secretKey.getBytes());
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");

        encoder =new CryptString(keyFactory.generateSecret(key));

    }

    //Compares the char array "other" with the encrypted password
    //The char array is converted to a String to add salt, the result is encrypted and then compared with the encrypted password
    public boolean isEqual(char [] other){
        try {
            String otherString = new String(other);
            String toCrypt= otherString + salt;
            String criptedString = encoder.encryptBase64(toCrypt);

            if(criptedString.compareTo(encryptedPassword)==0)
                return true;
            else
                return false;


        }catch (Exception e) {
            System.err.println("Error:"+e.toString());
        }
        return false;// andrebbe tolto questo
    }

    //Getters
    public long getElapsedTime(){
        return (endTime-startTime);
    }
    public long getOverhead(){return(endTimeOverhead-startTimeOverhead);}
    public String getDecryptedPassword() {
        return decryptedPassword;
    }


    //Methods used in bruteForceTrivial

    //copy source, element by element, inside dest
    private void copy(char [] source, char [] dest){
        for (int i=0; i<passwordSize; i++){
            dest[i]=source[i];
        }


    }

    //This method handles the char sequence sliding when passing from a subset to another, for example from lower cases to upper cases
    public  char getNewChar(char oldChar){
        char newChar;
        if (oldChar == 'z') {
            newChar='A';
        }else{
            if(oldChar== 'Z')
                newChar='.';
            else
                newChar=++oldChar;

        }
        return newChar;
    }


    //Methods used in bruteForceElegant

    //Convert a single int value  into a char
    public char convertSingleToChar(int toConvert){
        int converted =0;
        if(toConvert <= 25){
            converted = (int)('a') + toConvert;
        }else if( (toConvert > 25) && (toConvert <= 51)){
            converted = (int) ('A')+ toConvert-26;
        }else {
            converted = (int)('.')+toConvert-52;
        }
        return (char)converted;
    }

    //convert a number into the char sequence that that number indexes. E.g. 0->'aaaaaaaa'
    public  char[] convertoToChars(double initialChunk){
        initValue = new char [passwordSize];
        for (double i=0; i<passwordSize; i++){
            if(initialChunk > Math.pow(numOfChars,i)){

                int rest = (i==0) ? (int)(initialChunk% numOfChars):((int)(initialChunk / Math.pow(numOfChars,i))) %(int) numOfChars;
                if (rest == 0 && (initialChunk <= Math.pow(numOfChars,i+1))){
                    initValue[initValue.length-(int)i-1] = 'a';
                    initValue[initValue.length-(int)i-2] = 'b';
                    i++;
                }else{
                    initValue[initValue.length-(int)i-1] = convertSingleToChar(rest);
                }
            }else {
                initValue[initValue.length-(int)i-1] = 'a';
            }
        }

        return initValue;
    }



    //Methods used in DictionaryAttack

    //Read the file in path
    //Convert the passwordlist into an array of Strings
    public void acquirePasswordListAndConvert(String path)throws Exception {
        startTime=System.currentTimeMillis();
        startTimeOverhead=System.currentTimeMillis();
        List<String> commonPasswords= new ArrayList<String>();
        commonPasswords = Files.readAllLines(Paths.get(path));

        int size = commonPasswords.size();
        commonPasswordsArray = new String[size];
        Iterator<String> itr = commonPasswords.iterator();
        int i = 0;
        while (itr.hasNext()) {
            commonPasswordsArray[i] = itr.next();
            i++;
        }
        endTimeOverhead= System.currentTimeMillis();
    }

    //"Nested for" search
    public boolean bruteForceTrivial(){// l'aggiornamento del carattere è fatto con una funzione
        char []current = new char [passwordSize];
        copy(initValue, current);
        System.out.println("Starting the brutal search...");
        startTime=System.currentTimeMillis();
        for (int first = 0; first< numOfChars; first++){
            current [1]='a';
            for(int second = 0; second< numOfChars; second++){
                current[2]='a';
                for(int third = 0; third< numOfChars; third++){
                    current[3]='a';
                    for(int fourth = 0; fourth< numOfChars; fourth++){
                        current[4]='a';
                        for(int fifth = 0; fifth< numOfChars; fifth++){
                            current[5]='a';
                            for(int sixth = 0; sixth< numOfChars; sixth++){
                                current[6]='a';
                                for (int seventh = 0; seventh< numOfChars; seventh++){
                                    current[7] = 'a';
                                    //prova tutte le lettere minuscole
                                    for (int i = 0; i < 26; i++) {
                                        if(isEqual( current)){
                                            endTime=System.currentTimeMillis();
                                            decryptedPassword = new String(current);
                                            System.out.println("The password is "+ decryptedPassword);
                                            return true;
                                        }
                                        current[7]++;
                                    }
                                    // prova tutte le lettere maiuscole
                                    current[7] = 'A';
                                    for (int i = 0; i < 26; i++) {
                                        if(isEqual( current)){
                                            endTime=System.currentTimeMillis();
                                            decryptedPassword = new String(current);
                                            System.out.println("The password is "+ decryptedPassword);
                                            return true;
                                        }
                                        //System.out.println(new String (current));
                                        current[7]++;
                                    }
                                    //prova i numeri
                                    current[7] = '.';
                                    for (int i = 0; i < 12; i++) {
                                        if(isEqual( current)){
                                            endTime=System.currentTimeMillis();
                                            decryptedPassword = new String(current);
                                            System.out.println("The password is "+ decryptedPassword);
                                            return true;
                                        }
                                        //System.out.println(new String (current));
                                        current[7]++;
                                    }
                                    current[6]= getNewChar(current[6]);
                                }
                                current[5] = getNewChar(current[5]);
                            }
                            current[4] = getNewChar(current[4]);
                        }
                        current[3] = getNewChar(current[3]);
                    }
                    current[2] = getNewChar(current[2]);
                }
                current[1]= getNewChar(current[1]);
            }
            current[0] = getNewChar(current[0]);
        }
        System.out.println("Password not found");

        return false;
    }

    //"M function" search (see relation for M function definition)

    public boolean bruteForceElegant(){

        System.out.println("Starting the elegant search...");
        startTime=System.currentTimeMillis();
        double count =0;
        double current = initialChunk;
        char[] currentChars = new char[passwordSize];
        while( count <= numOfIteration){

            currentChars = convertoToChars(current);

            if(isEqual(currentChars)){
                endTime=System.currentTimeMillis();
                decryptedPassword = new String(currentChars);
                System.out.println("The password is "+ decryptedPassword +" and time is: "+getElapsedTime());
                return true;
            }else {
                current++;
                count++;
            }
        }
        return false;
    }

    //Dictionary Attack
    public boolean dictionaryAttack(String path) throws Exception{
        acquirePasswordListAndConvert(path);
        for(int i=0; i<commonPasswordsArray.length; i++){
            if(isEqual(commonPasswordsArray[i].toCharArray())){
                endTime=System.currentTimeMillis();
                decryptedPassword =commonPasswordsArray[i];
                endTime=System.currentTimeMillis();
                System.out.println("The password is "+ decryptedPassword);
                return true;
            }
        }
        return false;
    }




    public static void main (String [] args) {


    }


}
