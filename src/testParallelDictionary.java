import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

public class testParallelDictionary {
    public static void appendRow(FileWriter target, String pwd, long time, long overhead)throws IOException {
        System.out.println("pwd: "+pwd+", time: "+time);
        target.append(pwd+" "+time+" "+overhead+" \n");
    }

    public static String [] loadPasswords(String path)throws  IOException {
        List<String> passwordsT = Files.readAllLines(Paths.get(path));
        int size = passwordsT.size();
        String[] passwords = new String[size];
        Iterator<String> itr = passwordsT.iterator();
        int i = 0;
        while (itr.hasNext()) {
            passwords[i] = itr.next();
            i++;
        }
        return passwords;
    }
    public static  void main(String [] args) throws IOException, Exception {
        final File commonPasswordsFolder = new File("src/commonPasswords/");
        for (final File file : commonPasswordsFolder.listFiles()) {
            String fileWriterName= "csv/" +file.getName() + "parallelDictionary.csv";
            FileWriter fileWriter = new FileWriter(fileWriterName);

            String path = commonPasswordsFolder + "/"+ file.getName();

            String[] passwords = loadPasswords(path);
            int []indexes=new int [passwords.length];
            int nCycles=1000;
            int offset=passwords.length/nCycles;
            for(int j=0; j<nCycles; j++){
                indexes[j]=j*offset;
            }
            fileWriter.append("nPassword= " + passwords.length + "\n");
            fileWriter.append("Password time overhead\n ");
            for (int i = 0; i < nCycles; i++) {
                int nThreads=16;
                //int index = (int) (Math.random() * (passwords.length));
                int index= indexes[i];
                System.out.println("Index :" + index);
                String password = passwords[index];
                int passwordSize = password.length();
                DataBaseVictim dataBaseVictim = new DataBaseVictim("M5S", password, 8);
                DBAttackerParallel dbAttackerParallel= new DBAttackerParallel(dataBaseVictim,nThreads,path);
                dbAttackerParallel.attackDatabase();
                dbAttackerParallel.launchDictionaryAttack();
               /* DBAttackerSequential dbAttackerSequential = new DBAttackerSequential(dataBaseVictim, passwordSize);
                dbAttackerSequential.attackDatabase();
                dbAttackerSequential.dictionaryAttack(path);*/
                long elapsedTime = dbAttackerParallel.getElapsedTime();
                long overhead = dbAttackerParallel.getOverheadTime();
                appendRow(fileWriter, password, elapsedTime, overhead);
            }

            fileWriter.close();
        }
    }
}
