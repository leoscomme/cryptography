import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Logger;
import java.util.logging.Level;



public class testMultipleBrute {

    public static void appendRow(FileWriter target, String pwd, int numThreads, long time)throws IOException{
        System.out.println("pwd: "+pwd+", numThreads: "+numThreads+", time: "+time);
        target.append(pwd+" "+numThreads+" "+time+"\n");
    }

    public static  void main(String [] args){
        Logger logger = Logger.getAnonymousLogger();



        //multiThread brute force 4 threads
        try{
            FileWriter multipleElegantBrute = new FileWriter("multiElegant.csv");
            multipleElegantBrute.append("pwd numThreads time\n");

            int numThreads = 4;
            DataBaseVictim dataBaseVictim= new DataBaseVictim("M5S", "h8X2",8 ); //(64^8)/8
            DBAttackerParallel DBAttackerParallel = new DBAttackerParallel(dataBaseVictim,4,numThreads); //qaaaa99a(/4)... qaaaa99a(2*/4) ...Gaaaa99a(2*/4)
            DBAttackerParallel.attackDatabase();//64^8/16 eaaaa99a ... iaaaa99a 2*/16
            DBAttackerParallel.launchBruteForceElegant();//2*tot/16-10000 ...
            appendRow(multipleElegantBrute, DBAttackerParallel.getDecryptedPassword(),numThreads, DBAttackerParallel.getElapsedTime());

            dataBaseVictim= new DataBaseVictim("M5S", "p999",8 );
            DBAttackerParallel = new DBAttackerParallel(dataBaseVictim,4,numThreads);
            DBAttackerParallel.attackDatabase();
            DBAttackerParallel.launchBruteForceElegant();
            appendRow(multipleElegantBrute, DBAttackerParallel.getDecryptedPassword(),numThreads, DBAttackerParallel.getElapsedTime());

            dataBaseVictim= new DataBaseVictim("M5S", "qaa9",8 );
            DBAttackerParallel = new DBAttackerParallel(dataBaseVictim,4,numThreads);
            DBAttackerParallel.attackDatabase();
            DBAttackerParallel.launchBruteForceElegant();
            appendRow(multipleElegantBrute, DBAttackerParallel.getDecryptedPassword(),numThreads, DBAttackerParallel.getElapsedTime());

            dataBaseVictim= new DataBaseVictim("M5S", "ybPw",8 );
            DBAttackerParallel = new DBAttackerParallel(dataBaseVictim,4,numThreads);
            DBAttackerParallel.attackDatabase();
            DBAttackerParallel.launchBruteForceElegant();
            appendRow(multipleElegantBrute, DBAttackerParallel.getDecryptedPassword(),numThreads, DBAttackerParallel.getElapsedTime());

            dataBaseVictim= new DataBaseVictim("M5S", "F999",8 );
            DBAttackerParallel = new DBAttackerParallel(dataBaseVictim,4,numThreads);
            DBAttackerParallel.attackDatabase();
            DBAttackerParallel.launchBruteForceElegant();
            appendRow(multipleElegantBrute, DBAttackerParallel.getDecryptedPassword(),numThreads, DBAttackerParallel.getElapsedTime());

            dataBaseVictim= new DataBaseVictim("M5S", "Gaa9",8 );
            DBAttackerParallel = new DBAttackerParallel(dataBaseVictim,4,numThreads);
            DBAttackerParallel.attackDatabase();
            DBAttackerParallel.launchBruteForceElegant();
            appendRow(multipleElegantBrute, DBAttackerParallel.getDecryptedPassword(),numThreads, DBAttackerParallel.getElapsedTime());

            dataBaseVictim= new DataBaseVictim("M5S", "N9BB",8 );
            DBAttackerParallel = new DBAttackerParallel(dataBaseVictim,4,numThreads);
            DBAttackerParallel.attackDatabase();
            DBAttackerParallel.launchBruteForceElegant();
            appendRow(multipleElegantBrute, DBAttackerParallel.getDecryptedPassword(),numThreads, DBAttackerParallel.getElapsedTime());

            dataBaseVictim= new DataBaseVictim("M5S", "V999",8 );
            DBAttackerParallel = new DBAttackerParallel(dataBaseVictim,4,numThreads);
            DBAttackerParallel.attackDatabase();
            DBAttackerParallel.launchBruteForceElegant();
            appendRow(multipleElegantBrute, DBAttackerParallel.getDecryptedPassword(),numThreads, DBAttackerParallel.getElapsedTime());

            dataBaseVictim= new DataBaseVictim("M5S", "Waa9",8 );
            DBAttackerParallel = new DBAttackerParallel(dataBaseVictim,4,numThreads);
            DBAttackerParallel.attackDatabase();
            DBAttackerParallel.launchBruteForceElegant();
            appendRow(multipleElegantBrute, DBAttackerParallel.getDecryptedPassword(),numThreads, DBAttackerParallel.getElapsedTime());

            dataBaseVictim= new DataBaseVictim("M5S", "2ah.",8 );
            DBAttackerParallel = new DBAttackerParallel(dataBaseVictim,4,numThreads);
            DBAttackerParallel.attackDatabase();
            DBAttackerParallel.launchBruteForceElegant();
            appendRow(multipleElegantBrute, DBAttackerParallel.getDecryptedPassword(),numThreads, DBAttackerParallel.getElapsedTime());

            dataBaseVictim= new DataBaseVictim("M5S", "9999",8 );
            DBAttackerParallel = new DBAttackerParallel(dataBaseVictim,4,numThreads);
            DBAttackerParallel.attackDatabase();
            DBAttackerParallel.launchBruteForceElegant();
            appendRow(multipleElegantBrute, DBAttackerParallel.getDecryptedPassword(),numThreads, DBAttackerParallel.getElapsedTime());

            multipleElegantBrute.close();



        }catch (IOException e){
            logger.log(Level.SEVERE,"An IOexception was thrown", e);
        }
        catch (Exception e){
            logger.log(Level.SEVERE, "An exception was thrown", e);
        }

    }
}