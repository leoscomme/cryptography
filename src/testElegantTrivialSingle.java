import java.io.FileWriter;
import java.io.IOException;

public class testElegantTrivialSingle {

    public static void appendRow(FileWriter target, String pwd, long trivialTime, long elegantTime)throws IOException {
        System.out.println("pwd: "+pwd+", trivialTime: "+trivialTime+", elegantTime: "+elegantTime);
        target.append(pwd+" "+trivialTime+" "+elegantTime+"\n");
    }


    public static  void main(String [] args) throws IOException, Exception{
        FileWriter fileWriter = new FileWriter("singleTrivialElegant.csv");
        fileWriter.append("Password trivial elegant\n " );
        String [] passwords= {"aaaap999", "aaaaqaa9", "aaaaybPw", "aaaaF999", "aaaaGaa9", "aaaaN9BB", "aaaaV999", "aaaaWaa9", "aaaa2ah.", "aaaa9999"};

        for(int i=0; i<passwords.length; i++) {
            String password = passwords[i];
            System.out.println("Looking for password : "+ password);
            DataBaseVictim dataBaseVictim = new DataBaseVictim("M5S", password, 8);
            DBAttackerSequential dbAttackerSequential = new DBAttackerSequential(dataBaseVictim, 8);
            dbAttackerSequential.attackDatabase();

            dbAttackerSequential.bruteForceTrivial();
            String decryptedPassordTrivial = dbAttackerSequential.getDecryptedPassword();
            long elapsedTimeTrivial = dbAttackerSequential.getElapsedTime();

            dbAttackerSequential.bruteForceElegant();
            //String decryptedPassordElegant= dbAttackerSequential.getDecryptedPassword();
            long elapsedTimeElegant = dbAttackerSequential.getElapsedTime();
            appendRow(fileWriter, dbAttackerSequential.getDecryptedPassword(), elapsedTimeTrivial, elapsedTimeElegant);
        }
        fileWriter.close();


    }





}

