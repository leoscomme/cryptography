import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.Semaphore;

public class DBAttackerParallel {
    //Database attacked
    private DataBaseVictim dataBaseVictim;


    //Database retrivied data
    private String secretKey;
    private String salt;
    private String encryptedPassword;
    private int passwordSize;

    //number of thread
    private int nThreads;

    //attribute to launch a dictionary attack
    private List<String> commonPasswords;

    //search result
    private String decryptedPassword;
    //perfomance
    private Long elapsedTime;
    private Long overHeadTime=null;

    //Constructor to create an instance capable of all attack kinds
    public DBAttackerParallel(DataBaseVictim db, int passwordSize, int numOfThreads, String path)throws Exception{
        this.passwordSize = passwordSize;
        this.dataBaseVictim = db;
        this.nThreads = numOfThreads;
        this.commonPasswords = Files.readAllLines(Paths.get(path));
        //this.threads = new Thread[this.nThreads];
        this.decryptedPassword = null;
        this.elapsedTime = null;
    }
    //bruteForce only
    public DBAttackerParallel(DataBaseVictim db, int passwordSize, int numOfThreads)throws Exception{
        this.passwordSize = passwordSize;
        this.dataBaseVictim = db;
        this.nThreads = numOfThreads;
        this.commonPasswords = null;
        //this.threads = new Thread[this.nThreads];
        this.decryptedPassword = null;
        this.elapsedTime = null;
    }
    //Dictionary only
    public DBAttackerParallel(DataBaseVictim db, int numOfThreads, String path)throws Exception{
        this.passwordSize = 8;
        this.dataBaseVictim = db;
        this.nThreads = numOfThreads;
        this.commonPasswords = Files.readAllLines(Paths.get(path));
        //this.threads = new Thread[this.nThreads];
        this.decryptedPassword = null;
        this.elapsedTime = null;

    }

    //Simulates the SQLInjection and "steals" data from the DB and initialize the encoder with these information

    public void attackDatabase() throws Exception{
        System.out.println("Attacking the " +dataBaseVictim.getName()+ " database with a SQL Injection...");
        this.secretKey= dataBaseVictim.getSecretKey();
        this.encryptedPassword = dataBaseVictim.getHashedPassword();
        this.salt= dataBaseVictim.getSalt();
        System.out.println("Retrived information: secret key : "+secretKey+",  password :"+ encryptedPassword +",  salt : "+salt);
    }


    //Launches threads from the BruteForceElegantParallel class, which exploit the M funciton (see the relation)
    public void launchBruteForceElegant() throws Exception{
        BruteForceElegantParallel[] threads = new BruteForceElegantParallel[nThreads];
        double tot = Math.pow(64, passwordSize);
        double partial = tot / nThreads;
        try{
            for(int i =0; i< nThreads; i ++){
                threads[i] = new BruteForceElegantParallel(passwordSize, encryptedPassword,secretKey,salt,i*partial,partial);
            }
        }catch (Exception e){
            System.err.println("Error:"+e.toString());
        }

        for( int i=0; i<nThreads; i++){
            threads[i].start();
        }
        for(int i=0; i<nThreads; i++){
            threads[i].join();
            if(threads[i].getDecryptedPassword()!=null){
                decryptedPassword = threads[i].getDecryptedPassword();
                elapsedTime = threads[i].getElapsedTime();
            }
        }
    }

    //Launches threads from the DictionaryAttackParallel class, that perform a dictionary attack
    public void launchDictionaryAttack()throws Exception{
        System.out.println("Launching a Dictionary attack with "+nThreads+" threads");
        DictionaryAttackParallel[] threads = new DictionaryAttackParallel[nThreads];
        int dataSize= commonPasswords.size();
        int chunk= dataSize/nThreads;
        try{
            for(int i=0; i<nThreads; i++){
                threads[i]= new DictionaryAttackParallel(encryptedPassword,salt,secretKey,chunk,i,commonPasswords);
            }
        }catch (Exception e){
            System.err.println("Error:"+e.toString());
        }
        for(int i=0; i<nThreads; i++){
            threads[i].start();
        }
        for(int i=0; i<nThreads; i++){
            threads[i].join();
            if(threads[i].getDecryptedPassword()!=null){
                decryptedPassword = threads[i].getDecryptedPassword();
                elapsedTime = threads[i].getElapsedTime();
            }
        }

        for(int j=0; j<nThreads; j++){
            overHeadTime+=threads[j].getOverheadTime();
        }
        overHeadTime/=nThreads;

    }

    //Launches threads from the BruteForceTrivialParallel class, which exploit the nested for
    public void launchBruteForceTrivial() throws  Exception{
        System.out.println("Launching a Brute Force attack with "+nThreads+ " threads");
        BruteForceTrivialParallel[] threads = new BruteForceTrivialParallel[nThreads];
        if(64%nThreads!=0) {//tricky case
            int commonChunkSize = 64 / (nThreads - 1);
            int lastChunkSize = 64 % (nThreads - 1);
            for (int i = 0; i < nThreads - 1; i++) {
                System.out.println("Il thread " + i + " inizia da " + commonChunkSize * i + " per la durata di " + commonChunkSize);
                threads[i] = new BruteForceTrivialParallel(encryptedPassword,salt, secretKey, commonChunkSize * i, i, commonChunkSize);
            }
            threads[nThreads - 1] = new BruteForceTrivialParallel(encryptedPassword,salt, secretKey, commonChunkSize * (nThreads - 1), nThreads - 1, lastChunkSize);
            System.out.println("ultimo thread inizia da " + commonChunkSize * (nThreads - 1) + " per la durata di " + lastChunkSize);
        }else {
            int chunkSize = 64 / nThreads;
            for (int i = 0; i < nThreads; i++) {
                System.out.println("Il thread " + i + " inizia da " + chunkSize * i + " per la durata di " + chunkSize);
                threads[i] = new BruteForceTrivialParallel(encryptedPassword,salt, secretKey, chunkSize * i, i, chunkSize);
            }
        }
        for(int i=0; i<nThreads; i++){
            threads[i].start();
        }
        for(int i=0; i<nThreads; i++){
            threads[i].join();
            if(threads[i].getDecryptedPassword()!=null){
                decryptedPassword = threads[i].getDecryptedPassword();
                elapsedTime = threads[i].getElapsedTime();
            }
        }
        System.out.println("uscito");
    }


    //Getters
    public long getElapsedTime(){
        return elapsedTime;
    }
    public long getOverheadTime(){return overHeadTime;}
    public String getDecryptedPassword() {
        return decryptedPassword;
    }

    public static void main (String [] args) throws  Exception{
        DataBaseVictim dataBaseVictim= new DataBaseVictim("M5S", "qaabaaaaa",8 );
        //String path= System.getProperty("user.dir")+ "/src/10_million_password_list_top_100000.txt";
        DBAttackerParallel DBAttackerParallel = new DBAttackerParallel(dataBaseVictim,8,16);
        DBAttackerParallel.attackDatabase();
        DBAttackerParallel.launchBruteForceElegant();
        //DBAttackerParallel.launchDictionaryAttack();
     }
}
