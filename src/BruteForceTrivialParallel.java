import com.tima.crypto.CryptString;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.util.concurrent.Semaphore;


public class BruteForceTrivialParallel extends  Thread{

    private String encryptedPassword;
    private String salt;
    private CryptString encoder;

    int id;
    int chunkSize;

    private String decryptedPassword;
    private int nIterations;
    private long startTime;
    private long endTime;
    private char[] initValue;
    public static final int passwordSize=8;
    private static boolean foundIt = false;


    public BruteForceTrivialParallel(String encryptedPassword, String salt, String secretKey, int indexInitValue, int id, int chunkSize) throws Exception{

        this.encryptedPassword = encryptedPassword;
        this.decryptedPassword = null;
        this.id=id;
        this.salt=salt;
        foundIt = false;
        DESKeySpec keyP = new DESKeySpec(secretKey.getBytes());
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        this.encoder = new CryptString(keyFactory.generateSecret(keyP));
        startTime=0;
        endTime=0;
        nIterations=62;
        this.chunkSize=chunkSize;

        char first= getInitSingleChar(indexInitValue);

        String helper= first+"aaaaaaa";
        System.out.println("Thread "+id+" starts from "+helper);
        initValue=helper.toCharArray();

    }

    public long getElapsedTime(){
        return (endTime-startTime);
    }
    public String getDecryptedPassword() {
        return decryptedPassword;
    }

    private void copy(char [] a, char [] b){
        for (int i=0; i<passwordSize; i++){
            b[i]=a[i];
        }
    }
    public char getNewChar(char oldChar){
        char newChar;
        if (oldChar == 'z') {
            newChar='A';
        }else{
            if(oldChar== 'Z')
                newChar='.';
            else
                newChar=++oldChar;
        }
        return newChar;
    }


    public char getInitSingleChar(int toConvert){
        int converted =0;
        //toConvert = (int)toConvert;
        if(toConvert <= 25){
            converted = (int)('a') + toConvert;
        }else if( (toConvert > 25) && (toConvert <= 51)){
            converted = (int) ('A')+ toConvert-26;
        }else {
            converted = (int)('.')+toConvert-52;
        }
        return (char)converted;
    }

    public boolean isEqual(char [] a){
        try {
            String aString = new String(a); //oppure a.toString();
            String toCrypt= aString + salt;
            String criptedString = encoder.encryptBase64(toCrypt);
            //System.out.println("value : "+aString+" || hash : "+criptedString);
            if(criptedString.compareTo(encryptedPassword)==0)
                return true;
            else
                return false;

        }catch (Exception e) {
            System.err.println("Error:"+e.toString());
        }
        return false;// andrebbe tolto questo
    }


    public void run() {

            char[] current = new char[passwordSize];
            copy(initValue, current);
            System.out.println("Iniza la ricerca della password..");
            startTime = System.currentTimeMillis();
            for (int first = 0; first < chunkSize ; first++) {
                current[1] = 'a';
                for (int second = 0; second < nIterations; second++) {
                    current[2] = 'a';
                    for (int third = 0; third < nIterations; third++) {
                        current[3] = 'a';
                        for (int fourth = 0; fourth < nIterations; fourth++) {
                            current[4] = 'a';
                            for (int fifth = 0; fifth < nIterations; fifth++) {
                                current[5] = 'a';
                                for (int sixth = 0; sixth < nIterations; sixth++) {
                                    current[6] = 'a';
                                    for (int seventh = 0; seventh < nIterations; seventh++) {
                                        current[7] = 'a';
                                        //prova tutte le lettere minuscole
                                        for (int i = 0; i < 26 ; i++) {
                                            if(foundIt==true)
                                                return;
                                            if (isEqual(current)) {
                                                endTime = System.currentTimeMillis();
                                                foundIt=true;
                                                decryptedPassword = new String(current);
                                                System.out.println("The password is " + decryptedPassword + " founded in " + getElapsedTime() + " ms, founded by thread " + id);
                                                return;
                                            }
                                            current[7]++;
                                        }
                                        // prova tutte le lettere maiuscole
                                        current[7] = 'A';
                                        for (int i = 0; i < 26 ; i++) {
                                            if(foundIt==true)
                                                return;
                                            if (isEqual(current)) {
                                                endTime = System.currentTimeMillis();
                                                foundIt=true;
                                                decryptedPassword = new String(current);
                                                System.out.println("The password is " + decryptedPassword + " founded in " + getElapsedTime() + " ms, founded by thread " + id);
                                                return;
                                            }
                                            //System.out.println(new String (current));
                                            current[7]++;
                                        }
                                        //prova i numeri
                                        current[7] = '.';
                                        for (int i = 0; i < 12 ; i++) {
                                            if(foundIt==true)
                                                return;
                                            if (isEqual(current)) {
                                                endTime = System.currentTimeMillis();
                                                foundIt=true;
                                                decryptedPassword = new String(current);
                                                System.out.println("The password is " + decryptedPassword + " founded in " + getElapsedTime() + " ms, founded by thread " + id);
                                                return;
                                            }
                                            //System.out.println(new String (current));
                                            current[7]++;
                                        }
                                        current[6] = getNewChar(current[6]);
                                    }
                                    current[5] = getNewChar(current[5]);
                                }
                                current[4] = getNewChar(current[4]);
                            }
                            current[3] = getNewChar(current[3]);
                        }
                        current[2] = getNewChar(current[2]);
                    }
                    current[1] = getNewChar(current[1]);
                }
                current[0] = getNewChar(current[0]);
            }
            endTime = System.currentTimeMillis();
            System.out.println("Password not found by thread: "+Thread.currentThread().getId());
            return;
    }



    public static void main (String [] args) throws  Exception{
        int numThreads = 4;
        DataBaseVictim dataBaseVictim= new DataBaseVictim("M5S", "aaaaa/./",8 ); //(64^8)/8
        DBAttackerParallel DBAttackerParallel = new DBAttackerParallel(dataBaseVictim,4,numThreads); //qaaaa99a(/4)... qaaaa99a(2*/4) ...Gaaaa99a(2*/4)
        DBAttackerParallel.attackDatabase();//64^8/16 eaaaa99a ... iaaaa99a 2*/16
        DBAttackerParallel.launchBruteForceTrivial();
    }




    }


