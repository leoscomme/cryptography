import com.tima.crypto.CryptString;


import javax.crypto.*;
import javax.crypto.spec.*;
import java.security.SecureRandom;
public class DataBaseVictim {
    private String name;
    private String secretKey;
    private String hashedPassword;
    private String salt;
    private String password;
    private CryptString encoder;

    public DataBaseVictim (String name, String password, int saltSize) throws Exception{
        this.name=name;
        this.password=password;
        SecureRandom secureRandom= new SecureRandom();
        //Compute salt
        char [] saltChar= new char[saltSize];
        int i=0;
        while(i<saltSize){
            int value = secureRandom.nextInt()%126;
            while(value <33){
                value = secureRandom.nextInt()%126;
            }
            saltChar[i]= (char)value;
            i++;
        }
        salt= new String(saltChar);
        String toCrypt= password + salt;
        secretKey="00000000"; // va generata random
        DESKeySpec key = new DESKeySpec(secretKey.getBytes());
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        encoder = new CryptString(keyFactory.generateSecret(key));
        String hashWithoutSalt= encoder.encryptBase64(password);
        hashedPassword= encoder.encryptBase64(toCrypt);
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public String getName() {
        return name;
    }

    public String getSalt() {
        return salt;
    }
    public static  void main(String [] args) {
        try {
            DataBaseVictim dbv = new DataBaseVictim("M5S", "Hello", 5);
            System.out.println("hashed password : " + dbv.getHashedPassword());
            System.out.println("secret key : " + dbv.getSecretKey());
            System.out.println("salt : " + dbv.getSalt());
        }catch ( Exception e){

        }


    }

}
