package criptography;


// ORDINE DI SCORRIMENTO: minuscole, maiuscole, numeri;

public class SingleThread {
    //classe per l'esecuzione del programma in modo sequenziale, il nome si dovrà cambiare


    private char[] initValue;// per inizializzare tutto come una stringa 'aaaaaaaaa'
    private long startTime;
    private long endTime;
    private String result;
    private char[] key;
    private int nIterations;// numero di iterazioni per ogni ciclo for


    public SingleThread(String key){
        this.key= key.toCharArray();
        System.out.println("Looking for the password :"+new String(this.key));
        this.startTime=0;
        this.endTime=0;
        nIterations=62;// Le cifre sono complessivamente 62, (26+26+10)
        initValue= new char[size];// in teoria la struttura della classe dovrebbe essere tale che, variano l'init char, sia possibile avere anche l'implementazione parallela
                               // forse necessaria una funzione initialize value.
        for (int i=0; i< initValue.length; i++){
            initValue[i]= 'a';// usare '' per i char, "" per strighe
        }

    }
    public static final int size=8;// dimensione della chiave

    public boolean findPassword(){

        char [] current= new char[size];// Array che assume tutti i valori da 'aaaaaaaa' a '99999999'
        if(isEqual(key,initValue)){
             System.out.println("Risultato trovato al primo passo");
            result= new String(initValue);

            return true;
        }


         //copia il valore di key in current, da qui inizia ad iterare

         copy(initValue,current);


        System.out.println("Iniza la ricerca della password..");
        startTime=System.currentTimeMillis();
        for (int first=0; first<nIterations; first++){
            current [1]='a';
            for(int second=0; second<nIterations; second++){
                current[2]='a';
                for(int third=0; third<nIterations; third++){
                    current[3]='a';
                    for(int fourth=0; fourth<nIterations; fourth++){
                        current[4]='a';
                        for(int fifth=0; fifth<nIterations; fifth++){
                            current[5]='a';
                            for(int sixth =0; sixth<nIterations; sixth++){
                                current[6]='a';
                                for (int seventh=0; seventh<nIterations; seventh++){

                                        current[7] = 'a';
                                        //prova tutte le lettere minuscole
                                        for (int i = 0; i < 26; i++) {
                                            if(isEqual(key, current)){
                                                endTime=System.currentTimeMillis();
                                                result= new String(current);
                                                System.out.println("The password is "+result);

                                                return true;
                                            }

                                            current[7]++;


                                        }
                                    // prova tutte le lettere maiuscole

                                        current[7] = 'A';
                                        for (int i = 0; i < 26; i++) {
                                            if(isEqual(key, current)){
                                                endTime=System.currentTimeMillis();
                                                result= new String(current);
                                                System.out.println("The password is "+result);
                                                return true;
                                            }

                                            //System.out.println(new String (current));

                                            current[7]++;

                                        }
                                        //prova i numeri
                                        current[7] = '0';
                                        for (int i = 0; i < 10; i++) {
                                            if(isEqual(key, current)){
                                                endTime=System.currentTimeMillis();
                                                result= new String(current);
                                                System.out.println("The password is "+result);

                                                return true;
                                            }
                                            //System.out.println(new String (current));


                                            current[7]++;
                                        }



                                    if (current[6] == 'z') {
                                        current[6]='A';
                                    }else{
                                        if(current[6]== 'Z')
                                            current[6]='0';
                                        else
                                            current[6]++;
                                    }


                                }
                                if (current[5] == 'z') {
                                    current[5]='A';
                                }else{
                                    if(current[5]== 'Z')
                                        current[5]='0';
                                    else
                                        current[5]++;
                                }
                            }
                            if (current[4] == 'z') {
                                current[4]='A';
                            }else{
                                if(current[4]== 'Z')
                                    current[4]='0';
                                else
                                    current[4]++;
                            }
                        }
                        if (current[3] == 'z') {
                            current[3]='A';
                        }else{
                            if(current[3]== 'Z')
                                current[3]='0';
                            else
                                current[3]++;
                        }
                    }
                    if (current[2] == 'z') {
                        current[2]='A';
                    }else{
                        if(current[2]== 'Z')
                            current[2]='0';
                        else
                            current[2]++;
                    }

                }
                if (current[1] == 'z') {
                    current[1]='A';
                }else{
                    if(current[1]== 'Z')
                        current[1]='0';
                    else
                        current[1]++;
                }


            }
            if (current[0] == 'z') {
                current[0]='A';
            }else{
                if(current[0]== 'Z')
                    current[0]='0';
                else
                    current[0]++;
            }


        }
        System.out.println("Password non trovata");
        return false;

    }
    public boolean isEqual(char [] a, char [] b){// sarebbe private, messo public per il testMultipleBrute
        for(int i=0; i<size; i++){
            if(a[i]!=b[i])
                return false;
        }
        return true;
    }


    public long getElapsedTime(){
        return (endTime-startTime);
    }

    private void copy(char [] a, char [] b){
        for (int i=0; i<size; i++){
            b[i]=a[i];
        }
    }

    public String getResult() {
        return new String (result);
    }

    public static void main(String [] args){
        //String key ="aaaaaaaa";
        //SingleThread st= new SingleThread(key);


        //TEST sulla funzione isEqual
        /*char [] k= key.toCharArray();
        System.out.println(k);
        char[] b= new char[8];
        b[0]='1';
        b[1]='a';
        b[2]='a';
        b[3]='a';
        b[4]='e';
        b[5]='f';
        b[6]='g';
        b[7]='h';
        if(st.isEqual(k,b)){
            System.out.println("Is equal funziona");
        }*/

        //TEST sull'iterazione dei caratteri
        /*char [] b=key.toCharArray();


            int count=0;
            b[7] = 'a';
            for (int i = 0; i < 26; i++) {
                count++;
                System.out.flush();
                System.out.println(b);
                b[7]++;

            }
            b[7] = 'A';
            for (int i = 0; i < 26; i++) {
                count++;
                System.out.flush();
                System.out.println(b);
                b[7]++;

            }
            b[7] = '0';
            for (int i = 0; i < 10; i++) {
                count++;
                System.out.flush();
                System.out.println(b);
                b[7]++;
            }
            b[6]++;
            if(b[6]=='z'){
                b[6]='A';
            }
            if(b[6]=='Z'){
                b[6]='1';
            }
            System.out.println(count);*/



        //TEST funzionamento con variazioni di due caratteri
        String temp="aabbu9Zz";

        SingleThread st1= new SingleThread(temp);
        boolean result =st1.findPassword();
        //System.out.println(result);
        if(result) {
            System.out.println("Elapsed time " + st1.getElapsedTime() + " milliseconds");
        }
    }

}


