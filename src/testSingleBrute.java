import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class testSingleBrute {

    public static void appendRow(FileWriter target, String pwd, int numThreads, long time) throws IOException {
        System.out.println("pwd: " + pwd + ", numThreads: " + numThreads + ", time: " + time);
        target.append(pwd + " " + numThreads + " " + time + "\n");
    }

    public static void main(String[] args) {
        Logger logger = Logger.getAnonymousLogger();


        //sequential brute force
        try{
            FileWriter sequentialElegantBrute = new FileWriter("SequentialElegant.csv");
            sequentialElegantBrute.append("pwd numThreads time\n");

            DataBaseVictim dataBaseVictim= new DataBaseVictim("M5S", "h8X2",8 );
            DBAttackerSequential DBAttackerSequential = new DBAttackerSequential(dataBaseVictim,4);
            DBAttackerSequential.attackDatabase();
            DBAttackerSequential.bruteForceElegant();
            appendRow(sequentialElegantBrute, DBAttackerSequential.getDecryptedPassword(),1, DBAttackerSequential.getElapsedTime());

            dataBaseVictim= new DataBaseVictim("M5S", "p999",8 );
            DBAttackerSequential = new DBAttackerSequential(dataBaseVictim,4);
            DBAttackerSequential.attackDatabase();
            DBAttackerSequential.bruteForceElegant();
            appendRow(sequentialElegantBrute, DBAttackerSequential.getDecryptedPassword(),1, DBAttackerSequential.getElapsedTime());

            dataBaseVictim= new DataBaseVictim("M5S", "qaa/",8 );
            DBAttackerSequential = new DBAttackerSequential(dataBaseVictim,4);
            DBAttackerSequential.attackDatabase();
            DBAttackerSequential.bruteForceElegant();
            appendRow(sequentialElegantBrute, DBAttackerSequential.getDecryptedPassword(),1, DBAttackerSequential.getElapsedTime());

            dataBaseVictim= new DataBaseVictim("M5S", "ybPw",8 );
            DBAttackerSequential = new DBAttackerSequential(dataBaseVictim,4);
            DBAttackerSequential.attackDatabase();
            DBAttackerSequential.bruteForceElegant();
            appendRow(sequentialElegantBrute, DBAttackerSequential.getDecryptedPassword(),1, DBAttackerSequential.getElapsedTime());

            dataBaseVictim= new DataBaseVictim("M5S", "F999",8 );
            DBAttackerSequential = new DBAttackerSequential(dataBaseVictim,4);
            DBAttackerSequential.attackDatabase();
            DBAttackerSequential.bruteForceElegant();
            appendRow(sequentialElegantBrute, DBAttackerSequential.getDecryptedPassword(),1, DBAttackerSequential.getElapsedTime());

            dataBaseVictim= new DataBaseVictim("M5S", "Gaa/",8 );
            DBAttackerSequential = new DBAttackerSequential(dataBaseVictim,4);
            DBAttackerSequential.attackDatabase();
            DBAttackerSequential.bruteForceElegant();
            appendRow(sequentialElegantBrute, DBAttackerSequential.getDecryptedPassword(),1, DBAttackerSequential.getElapsedTime());

            dataBaseVictim= new DataBaseVictim("M5S", "N9BB",8 );
            DBAttackerSequential = new DBAttackerSequential(dataBaseVictim,4);
            DBAttackerSequential.attackDatabase();
            DBAttackerSequential.bruteForceElegant();
            appendRow(sequentialElegantBrute, DBAttackerSequential.getDecryptedPassword(),1, DBAttackerSequential.getElapsedTime());

            dataBaseVictim= new DataBaseVictim("M5S", "V999",8 );
            DBAttackerSequential = new DBAttackerSequential(dataBaseVictim,4);
            DBAttackerSequential.attackDatabase();
            DBAttackerSequential.bruteForceElegant();
            appendRow(sequentialElegantBrute, DBAttackerSequential.getDecryptedPassword(),1, DBAttackerSequential.getElapsedTime());

            dataBaseVictim= new DataBaseVictim("M5S", "Zaa/",8 );
            DBAttackerSequential = new DBAttackerSequential(dataBaseVictim,4);
            DBAttackerSequential.attackDatabase();
            DBAttackerSequential.bruteForceElegant();
            appendRow(sequentialElegantBrute, DBAttackerSequential.getDecryptedPassword(),1, DBAttackerSequential.getElapsedTime());

            dataBaseVictim= new DataBaseVictim("M5S", "2ah.",8 );
            DBAttackerSequential = new DBAttackerSequential(dataBaseVictim,4);
            DBAttackerSequential.attackDatabase();
            DBAttackerSequential.bruteForceElegant();
            appendRow(sequentialElegantBrute, DBAttackerSequential.getDecryptedPassword(),1, DBAttackerSequential.getElapsedTime());

            dataBaseVictim= new DataBaseVictim("M5S", "9999",8 );
            DBAttackerSequential = new DBAttackerSequential(dataBaseVictim,4);
            DBAttackerSequential.attackDatabase();
            DBAttackerSequential.bruteForceElegant();
            appendRow(sequentialElegantBrute, DBAttackerSequential.getDecryptedPassword(),1, DBAttackerSequential.getElapsedTime());


            sequentialElegantBrute.close();

        }catch (IOException e){
            logger.log(Level.SEVERE,"An IOexception was thrown", e);
        }
        catch (Exception e){
            logger.log(Level.SEVERE, "An exception was thrown", e);
        }


    }
}