import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class testFullBrute {

    public static void appendRow(FileWriter target, String pwd, int numThreads, long time)throws IOException {
        System.out.println("pwd: "+pwd+", numThreads: "+numThreads+", time: "+time);
        target.append(pwd+" "+numThreads+" "+time+"\n");
    }

    public static  void main(String [] args){
        Logger logger = Logger.getAnonymousLogger();

        //multiThread brute force 16 threads 8 chars
        try{
            FileWriter multipleElegantBrute = new FileWriter("multiFullElegant.csv");
            multipleElegantBrute.append("pwd numThreads time\n");

            System.out.println("pass: caaaa99a");

            int numThreads = 16;
            DataBaseVictim dataBaseVictim= new DataBaseVictim("M5S", "caaaa99a",8 ); //(64^8)/8
            DBAttackerParallel DBAttackerParallel = new DBAttackerParallel(dataBaseVictim,8,numThreads); //qaaaa99a(/4)... qaaaa99a(2*/4) ...Gaaaa99a(2*/4)
            DBAttackerParallel.attackDatabase();//64^8/16 eaaaa99a ... iaaaa99a 2*/16
            DBAttackerParallel.launchBruteForceElegant();//2*tot/16-10000 ...
            appendRow(multipleElegantBrute, DBAttackerParallel.getDecryptedPassword(),numThreads, DBAttackerParallel.getElapsedTime());

            System.out.println("pass: d9999999");

            dataBaseVictim= new DataBaseVictim("M5S", "d9999999",8 );
            DBAttackerParallel = new DBAttackerParallel(dataBaseVictim,8,numThreads);
            DBAttackerParallel.attackDatabase();
            DBAttackerParallel.launchBruteForceElegant();
            appendRow(multipleElegantBrute, DBAttackerParallel.getDecryptedPassword(),numThreads, DBAttackerParallel.getElapsedTime());

            System.out.println("pass: eaaaa999");

            dataBaseVictim= new DataBaseVictim("M5S", "eaaaa999",8 );
            DBAttackerParallel = new DBAttackerParallel(dataBaseVictim,8,numThreads);
            DBAttackerParallel.attackDatabase();
            DBAttackerParallel.launchBruteForceElegant();
            appendRow(multipleElegantBrute, DBAttackerParallel.getDecryptedPassword(),numThreads, DBAttackerParallel.getElapsedTime());




            multipleElegantBrute.close();



        }catch (IOException e){
            logger.log(Level.SEVERE,"An IOexception was thrown", e);
        }
        catch (Exception e){
            logger.log(Level.SEVERE, "An exception was thrown", e);
        }

    }

}
