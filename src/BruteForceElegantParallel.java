import com.tima.crypto.CryptString;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

public class BruteForceElegantParallel extends Thread {
    private String salt;
    private String encryptedPassword;
    private String decryptedPassword;
    private double numOfChars;
    private double numOfIteration;
    private long startTime;
    private long endTime;
    private char[] initValue;
    private double initialChunk;
    private CryptString encoder;
    public int passwordSize;
    private static boolean foundIt = false;


    public BruteForceElegantParallel(int passwordSize, String encryptedPassword, String secretKey, String salt, double intialChunk, double numOfIt) throws Exception {
        this.passwordSize = passwordSize;
        this.salt = salt;
        this.encryptedPassword = encryptedPassword;
        this.decryptedPassword = null;
        foundIt=false;
        DESKeySpec keyP = new DESKeySpec(secretKey.getBytes());
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        this.encoder = new CryptString(keyFactory.generateSecret(keyP));
        startTime = 0;
        endTime = 0;
        numOfChars = 64;
        this.initialChunk = intialChunk;
        numOfIteration = numOfIt;
        initValue = convertoToChars(intialChunk);

    }
    public BruteForceElegantParallel(){
        numOfChars = 64;
    } // x debug

    public char getInitSingleChar(int toConvert){
        int converted = 0;
        if(toConvert <= 25){
            converted = (int)('a') + toConvert;
        }else if( (toConvert > 25) && (toConvert <= 51)){
            converted = (int) ('A')+ toConvert-26;
        }else {
            converted = (int)('.')+toConvert-52;
        }
        return (char)converted;
    }

    public  char[] convertoToChars(double initialChunk){
        initValue = new char [passwordSize];
        for (double i=0; i<passwordSize; i++){
            if(initialChunk > Math.pow(numOfChars,i)){
                int rest = (i==0) ? (int)(initialChunk% numOfChars):((int)(initialChunk / Math.pow(numOfChars,i))) %(int) numOfChars;
                if (rest == 0 && (initialChunk <= Math.pow(numOfChars,i+1))){
                    initValue[initValue.length-(int)i-1] = 'a';
                    initValue[initValue.length-(int)i-2] = 'b';
                    i++;
                }else{
                    initValue[initValue.length-(int)i-1] = getInitSingleChar(rest);
                }
            }else {
                initValue[initValue.length-(int)i-1] = 'a';
            }
        }

        return initValue;
    }


    public  char[] getInitValue2(double initialChunk){
        initValue = new char [passwordSize];
        for (double i=0; i<passwordSize; i++){
            if(initialChunk > i*(numOfChars *(numOfChars -1)-1)){
                int rest = (i==0) ? (int)(initialChunk%(numOfChars *(numOfChars -1)-1)):(int)(initialChunk /(i+1)*(numOfChars *(numOfChars -1)-1));
                if (rest == 0 && (initialChunk <= (i+1)*(numOfChars *(numOfChars -1)-1))){
                    initValue[initValue.length-(int)i-1] = 'b';
                }else{
                    initValue[initValue.length-(int)i-1] = getInitSingleChar(rest);
                }
            }else {
                initValue[initValue.length-(int)i-1] = 'a';
            }
        }

        return initValue;
    } //troiaio


    public long getElapsedTime() {
        return (endTime - startTime);
    }

    public String getDecryptedPassword() {
        return decryptedPassword;
    }

    private void copy(char[] a, char[] b) {
        for (int i = 0; i < passwordSize; i++) {
            b[i] = a[i];
        }
    }



    public char getNewChar(char oldChar) {
        char newChar;
        if (oldChar == 'z') {
            newChar = 'A';
        } else {
            if (oldChar == 'Z')
                newChar = '.';
            else
                newChar = ++oldChar;
        }
        return newChar;
    }


    public boolean isEqual(char [] a){
        try {
            String aString = new String(a);
            String toCrypt= aString + salt;
            String criptedString = encoder.encryptBase64(toCrypt);
            if(criptedString.compareTo(encryptedPassword)==0)
                return true;
            else
                return false;

        }catch (Exception e) {
            System.err.println("Error:"+e.toString());
        }
        return false;
    }




    @Override
    public void  run() {
        System.out.println("il thread "+Thread.currentThread().getId()+" iniza la ricerca della password..");
        startTime=System.currentTimeMillis();
        double count =0;
        double current = initialChunk;
        char[] currentChars = new char[passwordSize];
        while( count < numOfIteration && !foundIt ){

            currentChars = convertoToChars(current);

            if(isEqual(currentChars)){
                endTime=System.currentTimeMillis();
                foundIt=true;
                decryptedPassword = new String(currentChars);
                System.out.println("The password is "+ decryptedPassword +" and time is: "+getElapsedTime()+" founded by thread "+Thread.currentThread().getId());
                return;
            }else {
                current++;
                count++;
            }
        }
        endTime=System.currentTimeMillis();
        System.out.println("Password not found by thread: "+Thread.currentThread().getId());
        decryptedPassword = null;
        return;
    }






    public static void main (String [] args){
        int prova = 64;
        BruteForceElegantParallel magi = new BruteForceElegantParallel();
        char[] res = magi.convertoToChars(prova);
        System.out.println(new String (res));


        String password = "abcd1234";

        try {
            DESKeySpec key = new DESKeySpec(password.getBytes());
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            CryptString crypt = new CryptString(keyFactory.generateSecret(key));
            String unencryptedString = "qaaaa99j";
            String encryptedString = crypt.encryptBase64(unencryptedString);

            double tot = Math.pow(64, 4);
            double partial = tot / 4;


            Thread [] threads= new Thread[8];
            for(int i=0; i<8; i++){
                threads[i]=new BruteForceElegantParallel(4,encryptedString,password,"ciao",partial*i,partial);
                new String(((BruteForceElegantParallel) threads[0]).convertoToChars(5*partial));
            }
            for(int i=0; i<8; i++){
                threads[i].start();
            }


        }catch (Exception e){
            System.err.println("Error:"+e.toString());
        }

    }


}
