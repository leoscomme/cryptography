import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class testFullSingleBrute {

    public static void appendRow(FileWriter target, String pwd, int numThreads, long time) throws IOException {
        System.out.println("pwd: " + pwd + ", numThreads: " + numThreads + ", time: " + time);
        target.append(pwd + " " + numThreads + " " + time + "\n");
    }

    public static void main(String[] args) {
        Logger logger = Logger.getAnonymousLogger();


        //sequential brute force 8chars
        try{
            FileWriter sequentialElegantBrute = new FileWriter("SequentialElegantFull.csv");
            sequentialElegantBrute.append("pwd numThreads time\n");

            System.out.println("pass: caaaa99a");

            DataBaseVictim dataBaseVictim= new DataBaseVictim("M5S", "caaaa99a",8 );
            DBAttackerSequential DBAttackerSequential = new DBAttackerSequential(dataBaseVictim,8);
            DBAttackerSequential.attackDatabase();
            DBAttackerSequential.bruteForceElegant();
            appendRow(sequentialElegantBrute, DBAttackerSequential.getDecryptedPassword(),1, DBAttackerSequential.getElapsedTime());

            System.out.println("pass: d9999999");

            dataBaseVictim= new DataBaseVictim("M5S", "d9999999",8 );
            DBAttackerSequential = new DBAttackerSequential(dataBaseVictim,8);
            DBAttackerSequential.attackDatabase();
            DBAttackerSequential.bruteForceElegant();
            appendRow(sequentialElegantBrute, DBAttackerSequential.getDecryptedPassword(),1, DBAttackerSequential.getElapsedTime());

            System.out.println("pass: eaaaa999");

            dataBaseVictim= new DataBaseVictim("M5S", "eaaaa999",8 );
            DBAttackerSequential = new DBAttackerSequential(dataBaseVictim,4);
            DBAttackerSequential.attackDatabase();
            DBAttackerSequential.bruteForceElegant();
            appendRow(sequentialElegantBrute, DBAttackerSequential.getDecryptedPassword(),1, DBAttackerSequential.getElapsedTime());


            sequentialElegantBrute.close();

        }catch (IOException e){
            logger.log(Level.SEVERE,"An IOexception was thrown", e);
        }
        catch (Exception e){
            logger.log(Level.SEVERE, "An exception was thrown", e);
        }


    }

}
