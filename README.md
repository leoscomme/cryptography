# Parallel Brute-Force and Dictionary Attack

This repository simulates a parallel brute-force and a dictionary attack to a database.

### Prerequisites

To run this project is sufficient to clone the repository on your machine: additional jars and dictionary files ( one with 1 million passwords and one with 100.000) are already present.

## Getting Started

To start a sequential brute-force attack with trivial password sliding: 

* Create an object of type "DBAttackerSequential" giving in input an instance of a "DataBaseVictim" object and the password size (int)
* Launch the method "attackDatabase()" that simulates the SQLInjection, "steals" data from the DB and initialize the encoder with these information
* Launch the method "bruteForceTrivial()"

To start a sequential brute-force attack with elegant password sliding: 

* Create an object of type "DBAttackerSequential" giving in input an instance of a "DataBaseVictim" object and the password size (int)
* Launch the method "attackDatabase()" that simulates the SQLInjection, "steals" data from the DB and initialize the encoder with these information
* Launch the method "bruteForceElegant()"

To start a sequential dictionary attack: 

* Create an object of type "DBAttackerSequential" giving in input an instance of a "DataBaseVictim" object and the password size (int)
* Launch the method "attackDatabase()" that simulates the SQLInjection, "steals" data from the DB and initialize the encoder with these information
* Launch the method "dictionaryAttack(String path)" with the path of a txt file that contains a list of passwords

To start a parallel brute-force attack with trivial password sliding:

* Create an object of type "DBAttackerParallel" giving in input an instance of a "DataBaseVictim" object, the password size (int) and the number of Thread that you want to use (int)
* Launch the method "attackDatabase()" that simulates the SQLInjection, "steals" data from the DB and initialize the encoder with these information
* Launch the method "launchBruteForceTrivial()"

To start a parallel brute-force attack with elegant password sliding:

* Create an object of type "DBAttackerParallel" giving in input an instance of a "DataBaseVictim" object, the password size (int) and the number of Thread that you want to use (int)
* Launch the method "attackDatabase()" that simulates the SQLInjection, "steals" data from the DB and initialize the encoder with these information
* Launch the method "launchBruteForceElegant()"

To start a parallel dictionary attack:

* Create an object of type "DBAttackerParallel" giving in input an instance of a "DataBaseVictim" object, the number of Thread that you want to use (int) and the path to a txt file that contains a list of passwords 
* Launch the method "attackDatabase()" that simulates the SQLInjection, "steals" data from the DB and initialize the encoder with these information
* Launch the method "launchDictionaryAttack()"



## Running the tests

In this repository are present 7 test file ready to launch:

* testSingleBrute - sequential brute-force with password length of 4 characters
* testFullSingleBrute - sequential brute-force with password length of 8 characters
* testSingleDictionary - sequential dictionary attack 
* testElegantTrivialSingle - compares the versions of brute force: the elegant and the trivial one
* testMultipleBrute - multi thread brute force with 4 threads and passwords length of 4 characters
* testParallelDictionary - multi thread dictionary attack
* testFullBrute - multi thread brute force with 16 threads and passwords length of 8 characters


## Authors

* **Leonardo Scommegna** 
* **Cosimo Rulli**


